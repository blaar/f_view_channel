#include "blc_channel.h" //blc_channel

#define HEIGHT 600
#define WITH 800

void square(blc_mem *mem, uchar value, int x, int y, int width, int height){
    int i, j;
    
    FOR(i, width) FOR(j, height) mem->uchars[x+i+(y+j)*WITH]=value;
}

int main(int argc, char **argv){
    blc_channel channel;
    
    //Si on change de caracteristique il va y avoir un conflit avec l'ancienne image. Il faut changer de nom.
    blc_channel_create_or_open(&channel, "/image_squared", BLC_CHANNEL_WRITE, 'UIN8', 'Y800', 2, 800, 600);
    
    square(&channel.array.mem, 255, 0, 0, 800, 600);
    square(&channel.array.mem, 0, 100, 50, 100, 60);
    
    //Necessary when using another program reading it.
    printf("%s\n", channel.name);
    fflush(stdout);
    
    fprintf(stderr, "Press enter to quit\n");
    getchar();
    
    return 0;
}