# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)

project(f_view_channel)

#This is to be able to debug and recompile automatically the libs (shared_blc). It is slower, you can remove this line and use  :  ${BLAR_BUILD_DIR}/lib/libblc.dylib instead of shared_blc

find_package(blc_channel REQUIRED)    
find_package(blc_image REQUIRED)  
find_package(blc_program REQUIRED)
find_package(blgtk REQUIRED)

find_package(PkgConfig REQUIRED) 
pkg_check_modules(GTK3 REQUIRED gtk+-3.0)
find_package(JPEG REQUIRED) 

add_definitions(-Wall ${BL_DEFINITIONS} -Wno-deprecated-declarations) #device_manager (mouse) is deprecated
include_directories(${GTK3_INCLUDE_DIRS} ${BL_INCLUDE_DIRS} ${JPEG_INCLUDE_DIR})
link_directories(${GTK3_LIBRARY_DIRS})
add_definitions(${GTK3_CFLAGS_OTHER})
add_executable(f_view_channel src/main.cpp src/values_display.cpp src/image_display.cpp src/blgtk_channel_display.cpp)
target_link_libraries(f_view_channel ${GTK3_LIBRARIES} ${BL_LIBRARIES} ${JPEG_LIBRARIES} )





