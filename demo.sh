

echo
echo "Demo t_rectangle"
echo "================"
echo
echo "The t_rectangle program create blc_channel describing a white image with a black rectangle"
echo "Then f_view_channel is called to display the blc_channel"
echo

cd `dirname $0`/.. #We go in blaar directory

echo
echo "Executing:"
echo "cd $PWD"
echo "./run.sh f_view_channel/t_rectangle |./run.sh f_view_channel"

./run.sh f_view_channel/t_rectangle |./run.sh f_view_channel
echo