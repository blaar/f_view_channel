#include "main.hpp"

#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
//#include <jpeglib.h>
#include <sys/mman.h>
#include <errno.h> //errno
#include "blc_core.h"
#include "blc_program.h"
#include "blc_command.h"


#include "image_display.hpp"
#include "values_display.hpp"

const char *channel_name, *fullscreen_option, *keyboard_mode;
GtkWidget *window;
GdkDisplay *main_display;
GdkDevice *pointer_device;
int interactive_mode=0;
GtkApplication *app;
blc_channel mouse_channel;

blc_channel input;

char const *input_name;

int input_terminal;
int mode;

char const *format_string;
uint32_t format;

void ask_fullscreen(){
    gtk_window_fullscreen(GTK_WINDOW(window));
    
}

void ask_quit()
{
    g_application_quit(G_APPLICATION(app));
    input.~blc_channel();
    mouse_channel.~blc_channel();
}

void on_key_press(GtkWidget	     *widget, GdkEventKey	     *event){

    char key;
    
    if (event->type == GDK_KEY_PRESS){
        key=event->keyval;
        fwrite(&key, 1, 1, stdout);
        fflush(stdout);
    }
}


void activate_cb(GApplication *app)
{
    blc_channel *input;
    char tmp_title[NAME_MAX*2+1];
    GtkWidget *display=NULL;
    GtkWidget *grid;
    
    main_display = gdk_display_get_default ();
    GdkDeviceManager *device_manager = gdk_display_get_device_manager (main_display);
    pointer_device = gdk_device_manager_get_client_pointer (device_manager);
    
    window=gtk_application_window_new(GTK_APPLICATION(app));
    SPRINTF(tmp_title, "%s",blc_program_name);
    gtk_window_set_title(GTK_WINDOW(window), tmp_title);
    
    grid=gtk_grid_new();
    
  //  for(i=0; input_names[i]; i++){
        input=new blc_channel(/*input_names[i]*/ input_name, BLC_CHANNEL_READ);
        display=create_image_display(input);
        if (display==NULL){
       //     if (*mouse_channel.name) EXIT_ON_ERROR("You cannot have mouse position on a channel that is not a image"); ///@TODO We may imagine to get the array coordinates
            display=create_values_display(input, format);
        } else  if (*mouse_channel.name) mouse_channel.create_or_open(NULL, O_WRONLY, 0, 'UI16', 'NDEF', NULL, 1, 3);
        if (display==NULL) EXIT_ON_CHANNEL_ERROR(input, "Format not managed.");
        gtk_widget_set_hexpand(display, 1);
        gtk_widget_set_vexpand(display, 1);
        gtk_container_add(GTK_CONTAINER(grid), display);
  //  }
    gtk_container_add(GTK_CONTAINER(window), grid);
    gtk_widget_show_all(window);
    if (keyboard_mode) g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK (on_key_press), NULL);
    
    
    blc_command_interpret_thread("hq", ask_quit);
}

/** Classical GTK application.
 * The first optional argument is the name of the experience. Otherwise all the existing shared memory are used.
 * */
int main(int argc, char *argv[])
{
    int status=0;
    char const *g_debug, *write_flag, *mouse_name;
    blc_channel channel_info;
    
    blc_program_set_description("Display the content of the blc_channel depending on its type on format");
    blc_program_add_option(&format_string, 'f', "format", "TBOX|VMET|CBOX", "floats in text boxes, viewmeters or check boxes", "NDEF");
    blc_program_add_option(&keyboard_mode, 'k', "keyboard", NULL, "Send keyboard input to stdout", NULL);
    blc_program_add_option(&mouse_name, 'm', "mouse", "blc_channel", "return the mouse coordinates  and status in the channel", NULL);
    blc_program_add_option(&write_flag, 'w', "write", NULL, "open the channel in write mode", NULL);
    blc_program_add_option(&fullscreen_option, 'F', "fullscreen", NULL, "Set the window in fullscreen", NULL);
    blc_program_add_option(&g_debug, ' ', "g-fatal-warnings", NULL, "Debug gtk.", NULL);
 // This function is not yer operaiotnal  blc_program_add_multiple_parameters(&input_names, "blc_channel", 1, "channel name you want to display");
    blc_program_add_parameter(&input_name, "blc_channel", 1, "channel name you want to display", NULL);
    
    blc_program_init(&argc, &argv, NULL);
    
    if (write_flag) mode=BLC_CHANNEL_WRITE;
    else mode=BLC_CHANNEL_READ;
    
    input.open(input_name, mode);
    format=STRING_TO_UINT32(format_string);
    
    gtk_disable_setlocale();
    gtk_init(&argc, &argv);
    app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
    g_signal_connect (app, "activate", G_CALLBACK (activate_cb), NULL);
    status = g_application_run(G_APPLICATION(app), 0, NULL);
    g_object_unref(app);
    
    return (status);
}

