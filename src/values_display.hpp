#ifndef VALUES_DISPLAY_HPP
#define VALUES_DISPLAY_HPP
#include <gtk/gtk.h>
#include "blc_channel.h"

GtkWidget *create_values_display(blc_channel *channel, uint32_t format);


#endif