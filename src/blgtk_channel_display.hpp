#ifndef BLGTK_CHANNEL_DISPLAY_HPP
#define BLGTK_CHANNEL_DISPLAY_HPP

#include <gtk/gtk.h>
#include "blc_channel.h"

typedef struct blgtk_channel_display{
    blc_channel *channel;
    GValue *gvalues;
    int *columns_ids;
    int columns_nb;
    int editable;
    GtkTreeStore *tree_store;
    GtkWidget *widget;
    struct renderer_data *renderers_data;
    
    
    void destroy();
    void init_tree_store(GType gtype);
    void update();
    virtual void update_line(void *data, GtkTreeIter *iter)=0;
}blgtk_channel_display;

typedef struct blgtk_channel_display_text:blgtk_channel_display{
    blgtk_channel_display_text();
    blgtk_channel_display_text(blc_channel *channel);

    GtkWidget *create(blc_channel *channel);
    void update_line(void *data, GtkTreeIter *iter);
    
}blgtk_channel_display_text;

typedef struct blgtk_channel_display_progress:blgtk_channel_display{
    blgtk_channel_display_progress();
    blgtk_channel_display_progress(blc_channel *channel);

    GtkWidget *create(blc_channel *channel);
    void update_line(void *data, GtkTreeIter *iter);

}blgtk_channel_display_progress;

typedef struct blgtk_channel_display_toggle:blgtk_channel_display{
    blgtk_channel_display_toggle();
    blgtk_channel_display_toggle(blc_channel *channel);
    
    GtkWidget *create(blc_channel *channel);
    void update_line(void *data, GtkTreeIter *iter);
    
}blgtk_channel_display_toggle;

#endif