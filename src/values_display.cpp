#include "values_display.hpp"

#include "main.hpp"

#include <fcntl.h> // O_RDONLY ...
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <stdint.h> //uint32_t
#include <float.h>
#include <sys/mman.h>
#include <errno.h> //errno
#include "blc_core.h"
#include "blgtk.h"
#include "blgtk_channel_display.hpp"

blgtk_channel_display *channel_display=NULL;

GtkToolButton *display_channel_toggled=NULL;

typedef void (*type_update_widget_function)(GtkWidget*, gdouble);
typedef void (*type_update_channel_function)(GtkWidget*, gdouble);

struct renderer_data{
    GtkTreeModel *tree_model;
    int column_id;
    char* offset_data;
    int line_step;
    int type;
};

struct update
{
    GtkWidget *main_widget;
    type_update_widget_function function;
    uint32_t type;
    void **values;
    int values_nb;
    GtkWidget **widgets;
    int widgets_nb;
    int g_source_mode;
    
    update(uint32_t type);
    void add(GtkWidget *widget, void *value);
    void update_all();
};

static guint refresh_timer_id;
static int refresh_period=500;


update::update(uint32_t type):main_widget(NULL), function(NULL), type(type), values(NULL), values_nb(0), widgets(NULL), widgets_nb(0), g_source_mode(G_SOURCE_CONTINUE){
}

void update::add(GtkWidget *widget, void *value_pt)
{
    APPEND_ITEM(&values, &values_nb, &value_pt);
    APPEND_ITEM(&widgets, &widgets_nb, &widget);
}

void update::update_all()
{
    int i;
    uint32_t type_tmp;
    
    switch (type)
    {
        case 'UIN8': FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(uchar*)values[i]);
            break;
        case 'INT8': FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(char*)values[i]);
            break;
        case 'FL32':
            FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(float*)values[i]);
            break;
        case 'IN32':
            FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(int32_t*)values[i]);
            break;
        case 'UI32':
            FOR_INV(i, values_nb) function(widgets[i], (gdouble)*(uint32_t*)values[i]);
            break;
        default:EXIT_ON_ERROR("type '%.4s' is not managed", UINT32_TO_STRING(type_tmp, type));
    }
}

float value_min=0, value_max=1;

void getting_range_float(GtkRange *range, float *value){
    *value = gtk_range_get_value(range);
}

void getting_range_uchar(GtkRange *range, uchar *value){
    *value = gtk_range_get_value(range);
}

void getting_range_char(GtkRange *range, char *value){
    *value = gtk_range_get_value(range);
}

void getting_spin_value(GtkSpinButton *button, float *value){
    *value = gtk_spin_button_get_value(button);
}

void getting_check_value(GtkToggleButton *button, float *value){
    if (gtk_toggle_button_get_active(button)) *value = 1.0;
    else *value = 0.0;
}

void update_entry(GtkWidget *entry, gdouble value){
    char entry_string[NAME_MAX];
    SPRINTF(entry_string, "%lf", value);
    gtk_entry_set_text(GTK_ENTRY(entry), entry_string);
}

void update_check_value(GtkWidget *button, gdouble value){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), (value > 0.5));
}

gboolean tick_callback(GtkRange *, GdkFrameClock *, struct update *update){
    update->update_all();
    return update->g_source_mode;
}




static void *refresh_update(void *user_data)
{
    int ch=' ';
    struct update *update=(struct update*)user_data;
    
    update->g_source_mode=G_SOURCE_REMOVE;
    while(ch!='q' && ch!=-1)
    {
        ch=fgetc(stdin);
        switch (ch){
                
            case '.':gtk_widget_add_tick_callback(update->main_widget, (GtkTickCallback)tick_callback, (void*)update, NULL);
                break;
            case 'q': g_application_quit(G_APPLICATION(app)); break;
            case '\n':break;
            case -1: if (feof(stdin)) exit(0);break;
            default:fprintf(stderr, "Unknown command '%c' code '%d'.\n", ch, ch);
        }
    }
    return NULL;
}

static void edited_cb(GtkCellRendererText *renderer, gchar *path, gchar *new_text, struct renderer_data *data)
{
    GtkTreeIter iter;
    int line_id;
    blc_mem mem;
    uint32_t type_str;
    
    line_id=*gtk_tree_path_get_indices(gtk_tree_path_new_from_string(path));
    mem.data=data->offset_data+data->line_step*line_id;
    
    gtk_tree_model_get_iter_from_string(data->tree_model,  &iter, path);
    gtk_tree_model_get(data->tree_model, &iter, data->column_id, mem.data, -1);
    
    switch (data->type) {
        case 'UIN8':*mem.uchars=strtol(new_text, NULL, 10);
            gtk_tree_store_set(GTK_TREE_STORE(data->tree_model), &iter, data->column_id, *mem.uchars, -1);
            break;
        case 'INT8':*mem.chars=strtol(new_text, NULL, 10);
            gtk_tree_store_set(GTK_TREE_STORE(data->tree_model), &iter, data->column_id, *mem.chars, -1);
            break;
        case 'FL32':*(mem.floats)=strtof(new_text, NULL);
            gtk_tree_store_set(GTK_TREE_STORE(data->tree_model), &iter, data->column_id, *mem.floats, -1);
            break;
        default:EXIT_ON_ERROR("type .4s not managed", UINT32_TO_STRING(type_str, data->type));break;
    }
}


static void refresh_cb(GtkToolButton *, blgtk_channel_display *display){

    display->update();
}

static gboolean repeat_refresh(gpointer user_data)
{
    channel_display->update();
    
    return G_SOURCE_CONTINUE;
}

static void repeat_refresh_cb(GtkToolButton *toolbutton, gpointer *user_data)
{
    if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toolbutton)))  refresh_timer_id=g_timeout_add(refresh_period, repeat_refresh, user_data);
    else g_source_remove(refresh_timer_id);
}

static void text_display_cb(GtkToolButton *toolbutton, GtkWidget *parent){
    blc_channel *channel;

    if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toolbutton))){
       
        channel=channel_display->channel;
        channel_display->destroy();
        FREE(channel_display);
        
        channel_display=new blgtk_channel_display_text(channel);
     
        channel_display->update();

        gtk_container_add(GTK_CONTAINER(parent), channel_display->widget);
        gtk_widget_show_all(channel_display->widget);
        
        if (display_channel_toggled) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(display_channel_toggled), FALSE);
        display_channel_toggled = toolbutton;
        
    }
}

static void progress_display_cb(GtkToolButton *toolbutton, GtkWidget *parent){

    blc_channel *channel;
    
    if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toolbutton))){
        channel=channel_display->channel;

        channel_display->destroy();
        FREE(channel_display);
        
        channel_display=new blgtk_channel_display_progress(channel);

        channel_display->update();

        gtk_container_add(GTK_CONTAINER(parent), channel_display->widget);
        gtk_widget_show_all(channel_display->widget);
        
        if (display_channel_toggled) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(display_channel_toggled), FALSE);

        display_channel_toggled = toolbutton;

    }
}

static void toggle_display_cb(GtkToolButton *toolbutton, GtkWidget *parent){
    blc_channel *channel;
    
    if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toolbutton))){
        channel = channel_display->channel;
        channel_display->destroy();
        FREE(channel_display);
        
        channel_display=new blgtk_channel_display_toggle(channel);
        channel_display->update();
        gtk_container_add(GTK_CONTAINER(parent), channel_display->widget);
        gtk_widget_show_all(channel_display->widget);
        
        if (display_channel_toggled) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(display_channel_toggled), FALSE);

        display_channel_toggled = toolbutton;
    }
}

GtkWidget *create_values_display(blc_channel *channel, uint32_t format){
    GtkWidget *vbox, *toolbar;
    char label_text[NAME_MAX + 1];
    GtkWidget *widget;
    GtkToggleToolButton *text_button, *progress_button, *toggle_button, *repeat_refresh_button;
    uint32_t type_str, format_str;
    GtkWidget  *scrolled_window;
    
    vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
    
    SPRINTF(label_text, "%s %.4s %.4s:", channel->name, UINT32_TO_STRING(type_str, channel->type), UINT32_TO_STRING(format_str, channel->format));
    if ((channel->mode & O_ACCMODE)==O_RDWR) strcat(label_text, "editable");
    widget = gtk_frame_new(label_text);
    
    toolbar = gtk_toolbar_new();
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);

    scrolled_window=gtk_scrolled_window_new(NULL, NULL);
    gtk_widget_set_hexpand(scrolled_window, 1);
    gtk_widget_set_vexpand(scrolled_window, 1);
    gtk_frame_set_shadow_type(GTK_FRAME(widget), GTK_SHADOW_IN);

    channel_display=new blgtk_channel_display_text(channel);
    
    gtk_container_add(GTK_CONTAINER(vbox), toolbar);
    gtk_container_add(GTK_CONTAINER(vbox), scrolled_window);
    gtk_container_add(GTK_CONTAINER(widget), vbox);
    gtk_container_add(GTK_CONTAINER(scrolled_window), channel_display->widget);

    blgtk_add_tool_button(toolbar, "refresh", "view-refresh", G_CALLBACK(refresh_cb), (void*)channel_display);
    repeat_refresh_button = blgtk_add_toggle_tool_button(toolbar, "repeat\nrefresh", "media-playlist-repeat", G_CALLBACK(repeat_refresh_cb), (void*)channel_display);

    text_button=blgtk_add_toggle_tool_button(toolbar, "text", "multimedia-volume-control", G_CALLBACK(text_display_cb), (void*)scrolled_window);
    if (channel->type !='UI16') progress_button=blgtk_add_toggle_tool_button(toolbar, "viewmeter", "multimedia-voulme-control", G_CALLBACK(progress_display_cb), (void*)scrolled_window);
     if (channel->type !='UI16') toggle_button=blgtk_add_toggle_tool_button(toolbar, "checkbox", "multimedia-volume-control", G_CALLBACK(toggle_display_cb), (void*)scrolled_window);
    
    if (format=='NDEF') format=channel->format;

    switch (format){
        case 'TBOX':case 'NDEF':gtk_toggle_tool_button_set_active(text_button, TRUE);break;
        case 'VMET':gtk_toggle_tool_button_set_active(progress_button, TRUE);break;
        case 'CBOX':gtk_toggle_tool_button_set_active(text_button, TRUE);break;
        default:EXIT_ON_CHANNEL_ERROR(channel, "format not defined. See also in th image_display");
    }
    
   if ((channel->mode & O_ACCMODE)==O_RDONLY) gtk_toggle_tool_button_set_active(repeat_refresh_button, TRUE);
    
    return widget;
}



