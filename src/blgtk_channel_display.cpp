#include "blgtk_channel_display.hpp"

#include "blc_core.h"
#include "blgtk.h"

struct renderer_data{
    GtkTreeModel *tree_model;
    int column_id;
    char* offset_data;
    int line_step;
    int type;
};

static void edited_cb(GtkCellRendererText *, gchar *path, gchar *new_text, struct renderer_data *data){
    GtkTreeIter iter;
    int line_id;
    blc_mem mem;
    uint32_t type_str;
    

    line_id=*gtk_tree_path_get_indices(gtk_tree_path_new_from_string(path));
    mem.data=data->offset_data+data->line_step*line_id;
    
    gtk_tree_model_get_iter_from_string(data->tree_model,  &iter, path);
    gtk_tree_model_get(data->tree_model, &iter, data->column_id, mem.data, -1);
    switch (data->type) {
        case 'UIN8':*mem.uchars=strtol(new_text, NULL, 10);
            gtk_tree_store_set(GTK_TREE_STORE(data->tree_model), &iter, data->column_id, *mem.uchars, -1);
            break;
        case 'INT8':*mem.chars=strtol(new_text, NULL, 10);
            gtk_tree_store_set(GTK_TREE_STORE(data->tree_model), &iter, data->column_id, *mem.chars, -1);
            break;
        case 'FL32':*(mem.floats)=strtof(new_text, NULL);
            gtk_tree_store_set(GTK_TREE_STORE(data->tree_model), &iter, data->column_id, *mem.floats, -1);
            break;
        default:EXIT_ON_ERROR("type .4s not managed", UINT32_TO_STRING(type_str, data->type));break;
    }
}

void blgtk_channel_display::destroy(){
    
    gtk_widget_destroy(GTK_WIDGET(widget));
    
    gtk_tree_store_clear(tree_store);
    
    //TODO destroy the tree_store itself
    FREE(gvalues);
    FREE(columns_ids);
}


void blgtk_channel_display::init_tree_store(GType gtype){
    GtkTreeIter iter;
    GtkCellRenderer *renderer;
    GType *value_types;
    int i, is_multidim;
    int mode;
    
    
    mode=fcntl(channel->fd, F_GETFL);
    
    if (mode==O_WRONLY || mode ==O_RDWR) editable=1;
    else editable=0;
    
    columns_nb=channel->dims[0].length;
    is_multidim = ( channel->dims_nb > 1 );
    
    //This will be used for update
    gvalues=MANY_ALLOCATIONS(columns_nb, GValue);
    columns_ids=MANY_ALLOCATIONS(columns_nb+is_multidim, int);
    
    //This is only used for gtk_tree_store_newv
    value_types=MANY_ALLOCATIONS(columns_nb+is_multidim, GType);
    FOR_INV(i, columns_nb){
        value_types[i] = gtype;
        G_VALUE_TYPE(&gvalues[i])=gtype;
        columns_ids[i]=i;
    }
    if (is_multidim) value_types[columns_nb]=G_TYPE_INT;
    tree_store=gtk_tree_store_newv(columns_nb+is_multidim, value_types);
    FREE(value_types);
    
    widget=gtk_tree_view_new_with_model(GTK_TREE_MODEL(tree_store));
    
    if (is_multidim){
        FOR(i, channel->dims[1].length) {
            gtk_tree_store_append(tree_store, &iter, NULL);
            gtk_tree_store_set(tree_store, &iter, columns_nb, i, -1);
        }
        renderer=gtk_cell_renderer_text_new();
        gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(widget), 0, "line", renderer,  "text", columns_nb, NULL);
        
    }  else gtk_tree_store_append(tree_store, &iter, NULL);
    
    gtk_tree_selection_set_mode(gtk_tree_view_get_selection(GTK_TREE_VIEW(widget)), GTK_SELECTION_SINGLE);
    
}

void blgtk_channel_display::update(){
    GtkTreeIter iter;
    size_t j;
    
    gtk_tree_model_get_iter_first (GTK_TREE_MODEL(tree_store), &iter);
    
    if (channel->dims_nb==1)  update_line(channel->data, &iter);
    else{
        FOR(j, channel->dims[1].length){
            update_line(channel->chars+j*channel->dims[1].step, &iter);
            gtk_tree_model_iter_next(GTK_TREE_MODEL(tree_store), &iter);
        }
    }
}

blgtk_channel_display_text::blgtk_channel_display_text(){}
blgtk_channel_display_text::blgtk_channel_display_text(blc_channel *channel){
    create(channel);
}

GtkWidget *blgtk_channel_display_text::create(blc_channel *channel){
    char column_name[NAME_MAX];
    GType gtype=0;
    GtkCellRenderer *renderer;
    size_t i;
    
    this->channel=channel;
    
    switch (channel->type){
        case 'UIN8': gtype= G_TYPE_UCHAR;break;
        case 'UI16': gtype= G_TYPE_UINT;break;
        case 'INT8': gtype = G_TYPE_CHAR;break;
        case 'FL32': gtype = G_TYPE_FLOAT;break;
        case 'FL64': gtype = G_TYPE_DOUBLE;break;
        default:EXIT_ON_CHANNEL_ERROR(channel, "Type not managed ");
    }
    
    init_tree_store(gtype);
    if ((channel->mode & O_ACCMODE)==O_RDWR) editable=1;
    
    // Not nice to have one rendere per column but how to get the column in the callback
    renderers_data=MANY_ALLOCATIONS(channel->dims[0].length, struct renderer_data);
    FOR(i, channel->dims[0].length){
        renderer = gtk_cell_renderer_text_new();
        renderers_data[i].tree_model=GTK_TREE_MODEL(tree_store);
        renderers_data[i].column_id=i;
        renderers_data[i].type=channel->type;
        if (channel->dims_nb==1) renderers_data[i].line_step=0;
        else renderers_data[i].line_step=channel->dims[1].step;
        renderers_data[i].offset_data=channel->chars+channel->dims[0].step*i;
        
        if (editable){
            g_object_set(G_OBJECT(renderer), "editable", 1, NULL);
            g_signal_connect(G_OBJECT(renderer), "edited", G_CALLBACK(edited_cb), (void*)&renderers_data[i]);
        }
        
        SPRINTF(column_name, "%lu", i);
        gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(widget), -1, column_name, renderer,  "text", i, NULL);
    }
    return widget;
}

void blgtk_channel_display_text::update_line(void *line_data, GtkTreeIter *iter){
    blc_mem  line;
    int i;
    
    line.data=line_data;
    switch (channel->type){
        case 'INT8':FOR(i, columns_nb) g_value_set_schar(&gvalues[i], line.chars[i]);break;
        case 'UIN8':FOR(i, columns_nb) g_value_set_uchar(&gvalues[i], line.uchars[i]);break;
        case 'UI16':FOR(i, columns_nb) g_value_set_uint(&gvalues[i], line.uints16[i]);break;
        case 'FL32':FOR(i, columns_nb) g_value_set_float(&gvalues[i], line.floats[i]);break;
        case 'FL64':FOR(i, columns_nb) g_value_set_double(&gvalues[i], line.doubles[i]);break;

        default:EXIT_ON_CHANNEL_ERROR(channel, "Type not managed");
    }
    line.data=NULL; //Avoid automatic free
    gtk_tree_store_set_valuesv(tree_store, iter, columns_ids, gvalues, columns_nb);
}

blgtk_channel_display_progress::blgtk_channel_display_progress(){};
blgtk_channel_display_progress::blgtk_channel_display_progress(blc_channel *channel){
    create(channel);
};


void progress_edited_cb(GtkCellRenderer *renderer,     GtkCellEditable *editable,   gchar           *path,   gpointer         user_data){
    GtkWidget *dialog;
    GtkWidget *view_meter;
    
    view_meter=	gtk_scale_button_new(GTK_ICON_SIZE_DIALOG, 0, 1, 0.001, NULL);
    dialog=gtk_dialog_new();
    
    gtk_container_add(GTK_CONTAINER(dialog), view_meter);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_show_all(dialog);
}

GtkWidget *blgtk_channel_display_progress::create(blc_channel *channel){
    char column_name[NAME_MAX];
    GType gtype=0, *value_types;
    GtkTreeIter iter;
    GtkCellRenderer *renderer;
    int is_multidim;
    size_t i;

    
    this->channel=channel;
    
    switch (channel->type){
        case 'UIN8': gtype= G_TYPE_UCHAR;break;
        case 'INT8': gtype = G_TYPE_CHAR;break;
        case 'FL32': gtype = G_TYPE_FLOAT;break;
        case 'FL64': gtype = G_TYPE_DOUBLE;break;
        default:EXIT_ON_CHANNEL_ERROR(channel, "Type not managed ");
    }
    
    columns_nb=channel->dims[0].length;
    is_multidim = ( channel->dims_nb > 1 );
    
    //This will be used for update. One over two column is the percentage !!
    gvalues=MANY_ALLOCATIONS(columns_nb*2, GValue);
    columns_ids=MANY_ALLOCATIONS(columns_nb*2+is_multidim, int);
    
    //This is only used for gtk_tree_store_newv
    value_types=MANY_ALLOCATIONS(columns_nb*2+is_multidim, GType);
    FOR_INV(i, columns_nb){
        value_types[i*2] = gtype; //This is for text
        value_types[i*2+1] = G_TYPE_INT; //This is for percentage
        
        G_VALUE_TYPE(&gvalues[i*2])=gtype;
        G_VALUE_TYPE(&gvalues[i*2+1])=G_TYPE_INT;
        
        columns_ids[i*2]=i*2;
        columns_ids[i*2+1]=i*2+1;

    }
    if (is_multidim) value_types[2*columns_nb]=G_TYPE_INT;
    tree_store=gtk_tree_store_newv(columns_nb*2+is_multidim, value_types);
    FREE(value_types);
    
    widget=gtk_tree_view_new_with_model(GTK_TREE_MODEL(tree_store));
    
    if (is_multidim){
        FOR(i, channel->dims[1].length) {
            gtk_tree_store_append(tree_store, &iter, NULL);
            gtk_tree_store_set(tree_store, &iter, columns_nb*2, i, -1);
        }
        renderer=gtk_cell_renderer_text_new();
        gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(widget), 0, "line", renderer,  "text", columns_nb*2, NULL);
        gtk_tree_view_column_set_expand(gtk_tree_view_get_column(GTK_TREE_VIEW(widget), 0), TRUE);
        
    }  else gtk_tree_store_append(tree_store, &iter, NULL);
    
   // gtk_tree_selection_set_mode(gtk_tree_view_get_selection(GTK_TREE_VIEW(widget)), GTK_SELECTION_SINGLE);
    
    // Not nice to have one rendere per column but how to get the column in the callback
    renderers_data=MANY_ALLOCATIONS(columns_nb, struct renderer_data);
    FOR(i, columns_nb){
        renderer = gtk_cell_renderer_progress_new();
   //     gtk_orientable_set_orientation(GTK_ORIENTABLE(renderer), GTK_ORIENTATION_VERTICAL);
   //     g_object_set(G_OBJECT(renderer), "inverted", TRUE, "height", 32, NULL);
           g_object_set(G_OBJECT(renderer), "mode", GTK_CELL_RENDERER_MODE_ACTIVATABLE, "height", 32, NULL);

        
        renderers_data[i].tree_model=GTK_TREE_MODEL(tree_store);
        renderers_data[i].column_id=i;
        renderers_data[i].type=channel->type;
        if (channel->dims_nb==1) renderers_data[i].line_step=0;
        else renderers_data[i].line_step=channel->dims[1].step;
        renderers_data[i].offset_data=channel->chars+channel->dims[0].step*i;
        
        SPRINTF(column_name, "%d", i);
        gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(widget), -1, column_name, renderer,   "text", 2*i, "value", 2*i+1, NULL);
        
     //   renderer->start_editing=progress_edited_cb;
        
       g_signal_connect(G_OBJECT(renderer), "editing-started", G_CALLBACK(progress_edited_cb), (void*)&renderers_data[i]);
        g_signal_connect(G_OBJECT(renderer), "activated", G_CALLBACK(progress_edited_cb), (void*)&renderers_data[i]);


    }
    return widget;
}



void blgtk_channel_display_progress::update_line(void *line_data, GtkTreeIter *iter){
    blc_mem  line;
    int i, percent;
    
    line.data=line_data;
    switch (channel->type){
        case 'INT8':FOR(i, columns_nb){
            g_value_set_schar(&gvalues[2*i], line.chars[i]);
            percent = line.chars[i]*100/128+50;
            percent= MIN(percent, 100);
            percent=MAX(percent, 0);
            g_value_set_int(&gvalues[2*i+1], percent);
        }
        break;
        case 'UIN8':FOR(i, columns_nb){
            g_value_set_uchar(&gvalues[2*i], line.chars[i]);
            percent = line.uchars[i]*100/256;
            percent= MIN(percent, 100);
            percent=MAX(percent, 0);
            g_value_set_int(&gvalues[2*i+1], percent);
        }
        break;
        case 'FL32':FOR(i, columns_nb){
            g_value_set_float(&gvalues[2*i], line.floats[i]); //Value
            percent = line.floats[i]*100;
            percent= CLAMP(percent, 0, 100);
            g_value_set_int(&gvalues[2*i+1], percent); //percentage
        }
            break;
        default:EXIT_ON_CHANNEL_ERROR(channel, "Type not managed");
    }
    gtk_tree_store_set_valuesv(tree_store, iter, columns_ids, gvalues, columns_nb*2);
}
/*
static void toggle_edited_cb(GtkCellRendererToggle *renderer, gchar *path, struct renderer_data *data)
{
    GtkTreeIter iter;
    int line_id;
    blc_mem mem;
    uint32_t type_str;
    gboolean active;
    
    line_id=*gtk_tree_path_get_indices(gtk_tree_path_new_from_string(path));
    mem.data=data->offset_data+data->line_step*line_id;
    
    gtk_tree_model_get_iter_from_string(data->tree_model,  &iter, path);
    gtk_tree_model_get(data->tree_model, &iter, data->column_id, mem.data, -1);
    
    active=gtk_cell_renderer_toggle_get_active (renderer);
    gtk_tree_store_set(GTK_TREE_STORE(data->tree_model), &iter, data->column_id, 1-active, -1);
    
    switch (data->type) {
        case 'UIN8': *mem.uchars= 1-active ? UINT8_MAX : 0; break;
        case 'INT8': *mem.chars= 1-active ? INT8_MAX : INT8_MIN; break;
        case 'FL32':*mem.floats = 1-active ? 1.0f : 0.0f; break;
        default:EXIT_ON_ERROR("type .4s not managed", UINT32_TO_STRING(type_str, data->type));break;
    }
}*/

blgtk_channel_display_toggle::blgtk_channel_display_toggle(){};
blgtk_channel_display_toggle::blgtk_channel_display_toggle(blc_channel *channel){
    create(channel);
};

void blgtk_channel_display_toggle::update_line(void *line_data, GtkTreeIter *iter){
    blc_mem  line;
    int i;
    
    line.data=line_data;
    switch (channel->type){
        case 'UIN8':FOR(i, columns_nb) g_value_set_boolean(&gvalues[i], (line.uchars[i]>=128));break;
        case 'INT8':FOR(i, columns_nb) g_value_set_boolean(&gvalues[i], (line.chars[i] > 0));break;
        case 'FL32':FOR(i, columns_nb) g_value_set_boolean(&gvalues[i], (line.floats[i]> 0.5));break;
        default:EXIT_ON_CHANNEL_ERROR(channel, "Type not managed");
    }
    gtk_tree_store_set_valuesv(tree_store, iter, columns_ids, gvalues, columns_nb);
}

GtkWidget *blgtk_channel_display_toggle::create(blc_channel *channel){
    char column_name[NAME_MAX];
    GtkCellRenderer *renderer;
    size_t i;
    
    this->channel=channel;
    
    init_tree_store(G_TYPE_BOOLEAN);
    
    // Not nice to have one rendere per column but how to get the column in the callback
    renderers_data=MANY_ALLOCATIONS(channel->dims[0].length, struct renderer_data);
    FOR(i, channel->dims[0].length){
        renderer = gtk_cell_renderer_toggle_new();
        g_object_set(G_OBJECT(renderer), "xalign", 0, NULL);

        
        renderers_data[i].tree_model=GTK_TREE_MODEL(tree_store);
        renderers_data[i].column_id=i;
        renderers_data[i].type=channel->type;
        if (channel->dims_nb==1) renderers_data[i].line_step=0;
        else renderers_data[i].line_step=channel->dims[1].step;
        renderers_data[i].offset_data=channel->chars+channel->dims[0].step*i;
        
        if (editable){
            g_object_set(G_OBJECT(renderer), "activatable", 1, NULL);

        
        }
        
        SPRINTF(column_name, "%uld", i);
        gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(widget), -1, column_name, renderer,  "active", i, NULL);
    }
    return widget;
}






