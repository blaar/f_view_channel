
gtk interface to view blc_channels

Install
========

OSX
---
need gtk3, blgtk basic library for gtk3,  will be installed

    brew install gtk+3 gnome-icon-theme
    git submodule add https://framagit.org/blaar/blgtk.git
    git submodule add https://framagit.org/blaar/f_view_channel.git
    ./install blgtk
    ./install 

Ubuntu
------
need gtk3, blgtk basic library for gtk3,  will be installed

    sudo apt-get install gtk+3
    git submodule add https://framagit.org/blaar/blgtk.git
    ./install.sh blgtk
    git submodule add https://framagit.org/blaar/f_view_channel.git
    ./install.sh f_view_channel
